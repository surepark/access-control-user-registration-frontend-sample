import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  form_email = new FormControl('');
  form2_email = new FormControl('');
  form2_code = new FormControl('');
  form2_password = new FormControl('');
  message = '';
  message2 = '';

  constructor(
    private auth: AuthService, 
  ) { }

  ngOnInit() {
  }

  public async forgotPassword(){
    try {
      const res = await this.auth.forgotPassword(this.form_email.value);
      console.log(res);
      this.message = 'Success';
    } catch (err) {
      console.log(err);
      this.message = 'Error: '+ err.message;
    }
  }

  public async updatePassword(){
    try {
      const res = await this.auth.forgotPasswordSubmit(
        this.form2_email.value,
        this.form2_code.value,
        this.form2_password.value);
      console.log(res);
      this.message2 = 'Success';
    } catch (err) {
      console.log(err);
      this.message2 = 'Error: '+ err.message;
    }
  }
}
