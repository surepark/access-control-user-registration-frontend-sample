import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Auth } from 'aws-amplify';
import { CognitoUserAttribute } from 'amazon-cognito-identity-js';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private cognitoUser:any;

  // Get access to window object in the Angular way
  private window: Window;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private http:HttpClient
  ) {
    this.window = this.document.defaultView;
  }

  public async signUp(username: string, password: string, 
    fullName: string, phoneNumber:string, department:string, token: string
) {
    const params = {
      username: username,
      password: password,
      attributes: {
        name: fullName,
        phone_number: phoneNumber,
        'custom:department': department
      },
      validationData: [
        new CognitoUserAttribute({ Name: "key", Value: token })
      ]
    };
    return await Auth.signUp(params);
  }

  public async logIn(username: string, password: string) {
    const params = {
      username: username,
      password: password,
    };
    this.cognitoUser = await Auth.signIn(params);
  }

  public async getSession() {
    return await Auth.currentSession();
  }

  public async forgotPassword(username:string){
    return await Auth.forgotPassword(username);
  }

  public async forgotPasswordSubmit(username:string, code:string, new_password:string){
    return await Auth.forgotPasswordSubmit(username, code, new_password);
  }

  public async getUserDetails() {
    if (!this.cognitoUser) {
      this.cognitoUser = await Auth.currentAuthenticatedUser();
    }
    return await Auth.userAttributes(this.cognitoUser);
  }

  public async getTokenfromInvitationId(invitationId:string){
    const URL = 'https://dev-api.surereserve.io/access-control-booking/invitations/';
    const data = await this.http.get(URL+invitationId).toPromise();
    console.log("Data: " + JSON.stringify(data));
    if (data['statusCode'] == 200){
      return data['body']['data'];
    } else{
      return ''; // Invalid
    }
  }

  public async updateNameAttribute(new_name:string, department:string, phone_number:string){
    if (!this.cognitoUser) {
      this.cognitoUser = await Auth.currentAuthenticatedUser();
    }
    return await Auth.updateUserAttributes(this.cognitoUser, {
      name: new_name,
      'custom:department': department,
      'phone_number': phone_number
    })
  }
}
