import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-login-get-user-info',
  templateUrl: './login-get-user-info.component.html',
  styleUrls: ['./login-get-user-info.component.css']
})
export class LoginGetUserInfoComponent implements OnInit {
  message2 = '';
  message3 = '';
  message4 = '';
  message5 = '';

  form2_email = new FormControl('');
  form2_password = new FormControl('');
  form3_name = new FormControl('');
  form3_department = new FormControl('');
  form3_phonenumber = new FormControl('');
  
  constructor(
    private auth: AuthService, 
  ) { }

  ngOnInit() {
  }

  public async onClickLogin(){
    try {
      await this.auth.logIn(
        this.form2_email.value, 
        this.form2_password.value
      );
      this.message2 = 'Success';
    } catch (err) {
      console.log(err);
      this.message2 = 'Error: '+ err.message;
    }
  }

  public async displayCredentials(){
    try {
      const userSessions = await this.auth.getSession();
      const token = userSessions.getIdToken().getJwtToken();
      this.message3 = token;
      console.log(token);
    } catch (err) {
      console.log(err);
      this.message3 = 'Error: '+ err.message;
    }
  }
  
  public async displayUserAttributes(){
    try {
      const userDetails = await this.auth.getUserDetails();
      this.message4 = '';
      userDetails.forEach(detail => {
        let name = detail.getName();
        let value = detail.getValue();
        console.log(name+": "+value);
        this.message4 += "{" + name + ": " + value + "}, ";
      });
    } catch (err) {
      console.log(err);
      this.message4 = 'Error: '+ err.message;
    }
  }

  public async updateUserAttributes(){
    try {
      this.message5 = '';
      const res = await this.auth.updateNameAttribute(
        this.form3_name.value,
        this.form3_department.value,
        this.form3_phonenumber.value
        );
      console.log(res);
      this.message5 = 'Success';
    } catch (err) {
      console.log(err);
      this.message5 = 'Error: '+ err.message;
    }
  }
}
