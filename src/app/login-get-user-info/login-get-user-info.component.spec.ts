import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginGetUserInfoComponent } from './login-get-user-info.component';

describe('LoginGetUserInfoComponent', () => {
  let component: LoginGetUserInfoComponent;
  let fixture: ComponentFixture<LoginGetUserInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginGetUserInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginGetUserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
