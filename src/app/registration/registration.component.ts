import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  invitation_id:string = '';
  token:string = '';
  token_valid = false;
  message = '';

  form1_email = new FormControl('');
  form1_password = new FormControl('');
  form1_token = new FormControl('');
  form1_name = new FormControl('');
  form1_phonenumber = new FormControl('');
  form1_department = new FormControl('');

  constructor(
    private auth: AuthService, 
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.invitation_id = this.route.snapshot.paramMap.get('id');
    console.log('invitation_id: ' + this.invitation_id);
    this.token = await this.auth.getTokenfromInvitationId(this.invitation_id);
    if (this.token == ''){
      this.token = 'INVALID TOKEN!';
    }
    console.log('token: ' + this.token);
  }
  
  public async onClickRegister(){
    try {
      // if (this.form1_token.value !='' && this.token == ''){
      //   this.token = this.form1_token.value;
      // }
      const res = await this.auth.signUp(
        this.form1_email.value, 
        this.form1_password.value,
        this.form1_name.value,
        this.form1_phonenumber.value,
        this.form1_department.value,
        this.token);
      console.log(res);
      this.message = 'Success';

    } catch (err) {
      console.log(err);
      if (err.code == 'InvalidLambdaResponseException'){
        this.message = 'Unauthorized signup';
      }
      else {
        this.message = err.message;
      }
    }
  }
}
